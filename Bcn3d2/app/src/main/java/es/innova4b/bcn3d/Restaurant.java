package es.innova4b.bcn3d;

import android.graphics.drawable.Drawable;import java.lang.Double;import java.lang.String;

/**
 * Created by usuari on 17/03/2015.
 */
public class Restaurant {

    Drawable imagen;
    String nombre;
    String descripcion;
    Double latitud;
    Double longitud;

    public Double getLatitud() {
        return latitud;
    }

    public void setLatitud(Double latitud) {
        this.latitud = latitud;
    }

    public Double getLongitud() {
        return longitud;
    }

    public void setLongitud(Double longitud) {
        this.longitud = longitud;
    }

    public Restaurant(Drawable imagen, String nombre, String descripcion, Double latitud, Double longitud) {
        this.imagen = imagen;
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.latitud=latitud;

        this.longitud=longitud;
    }

    public Drawable getImagen() {
        return imagen;
    }

    public void setImagen(Drawable imagen) {
        this.imagen = imagen;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
