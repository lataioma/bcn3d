package es.innova4b.bcn3d;



        import android.content.Context;
        import android.view.LayoutInflater;
        import android.view.View;
        import android.view.ViewGroup;
        import android.widget.BaseAdapter;
        import android.widget.Button;
        import android.widget.ImageView;
        import android.widget.TextView;
        import android.widget.Toast;

        import java.lang.Object;import java.lang.Override;import java.util.ArrayList;import edu.acme.app.printtext.R;

/**
 * Created by usuari on 11/03/2015.
 */
public class MonumentAdapter extends BaseAdapter
{
    Context context;

    static class ViewHolder
    {
        ImageView iv_monument;
        TextView nombre;
        TextView descripcion;
        Button buttonMapa;
        Button buttonReal;

    }

    //private static final String TAG = "CustomAdapter";
    //private static int convertViewCounter = 0;

    private ArrayList<Monumento> data;
    private LayoutInflater inflater = null;

    public MonumentAdapter(Context c, ArrayList<Monumento> d)



    {

        context=c;
        this.data = d;
        inflater = LayoutInflater.from(c);

    }

    @Override
    public int getCount()
    {

        return data.size();
    }

    @Override
    public Object getItem(int position)
    {

        return data.get(position);
    }

    @Override
    public long getItemId(int position)
    {

        return position;
    }

    @Override
    public int getViewTypeCount()
    {

        return 1;
    }

    @Override
    public int getItemViewType(int position)
    {

        return 0;
    }



    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {

        ViewHolder holder;


        if (convertView == null)
        {
            convertView = inflater.inflate(R.layout.lv_monumentos_row, null);
            holder = new ViewHolder();
            holder.iv_monument = (ImageView)convertView.findViewById(R.id.iv_monumento);
            holder.nombre = (TextView) convertView.findViewById(R.id.textViewNombre);
            holder.descripcion = (TextView) convertView.findViewById(R.id.textViewDescripcion);
            holder.buttonReal = (Button) convertView.findViewById(R.id.buttonReal);
            holder.buttonMapa = (Button) convertView.findViewById(R.id.buttonMapa);

            convertView.setTag(holder);

        } else
            holder = (ViewHolder) convertView.getTag();

        // Para porde hacer click en el checkbox
        Monumento d = (Monumento) getItem(position);
        holder.buttonMapa.setTag(d);
        holder.buttonReal.setTag(d);
        // Setting all values in listview
        holder.iv_monument.setImageDrawable(d.getImagen());
        holder.nombre.setText(d.getNombre());
        holder.descripcion.setText(d.getDescripcion());
        holder.buttonReal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context,"Pulsado realidad aumentada",Toast.LENGTH_LONG).show();
            }
        });
        holder.buttonMapa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context,"Pulsado Mapa",Toast.LENGTH_LONG).show();
            }
        });
        return convertView;
    }


    }



