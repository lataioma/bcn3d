package es.innova4b.bcn3d;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.lang.Object;import java.lang.Override;import java.util.ArrayList;

/**
 * Created by usuari on 20/03/2015.
 */
public class RestaurantAdapter extends BaseAdapter {

    Context context;

    static class ViewHolder
    {
        ImageView iv_restaurant;
        TextView nombre;
        TextView descripcion;

    }


    //private static final String TAG = "CustomAdapter";
    //private static int convertViewCounter = 0;

    private ArrayList<Restaurant> data;
    private LayoutInflater inflater = null;

    public RestaurantAdapter(Context c, ArrayList<Restaurant> d)



    {

        context=c;
        this.data = d;
        inflater = LayoutInflater.from(c);

    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder;


        if (convertView == null)
        {
            convertView = inflater.inflate(R.layout.lv_restaurantes_row, null);
            holder = new ViewHolder();
            holder.iv_restaurant = (ImageView)convertView.findViewById(R.id.iv_restaurante);
            holder.nombre = (TextView) convertView.findViewById(R.id.textViewNombre);
            holder.descripcion = (TextView) convertView.findViewById(R.id.textViewDescripcion);

            convertView.setTag(holder);

        } else
            holder = (ViewHolder) convertView.getTag();

        // Para porde hacer click en el checkbox
        Restaurant d = (Restaurant) getItem(position);
        holder.iv_restaurant.setImageDrawable(d.getImagen());
        holder.nombre.setText(d.getNombre());
        holder.descripcion.setText(d.getDescripcion());

        return convertView;
    }
}
