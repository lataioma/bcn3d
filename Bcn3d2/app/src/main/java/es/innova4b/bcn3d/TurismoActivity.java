package es.innova4b.bcn3d;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import java.util.ArrayList;


public class TurismoActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_turismo);


        ArrayList<ObjetoListView> listaObj = new ArrayList<>();

        listaObj.add(new ObjetoListView("pollo","948656"));
        listaObj.add(new ObjetoListView("alitas","56456456"));
        listaObj.add(new ObjetoListView("croquetas","45435"));
        listaObj.add(new ObjetoListView("muslitos","45435"));
        listaObj.add(new ObjetoListView("pechuga","45435"));

        ListView lv = (ListView) findViewById(R.id.listView);
        AdapterListView AE = new AdapterListView(this, listaObj);
        lv.setAdapter(AE);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_turismo, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
