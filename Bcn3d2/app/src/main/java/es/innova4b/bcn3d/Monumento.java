package es.innova4b.bcn3d;

import android.graphics.drawable.Drawable;import java.lang.String;

/**
 * Created by usuari on 11/03/2015.
 */
public class Monumento {
    Drawable imagen;
    String nombre;
    String descripcion;

    public Monumento(Drawable imagen, String nombre, String descripcion) {
        this.imagen = imagen;
        this.nombre = nombre;
        this.descripcion = descripcion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Drawable getImagen() {
        return imagen;
    }

    public void setImagen(Drawable imagen) {
        this.imagen = imagen;
    }
}