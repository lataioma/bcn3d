package es.innova4b.bcn3d;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.lang.Override;import java.util.ArrayList;

public class RestaurantActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_restaurantes);
       //ActionBar actionBar = getActionBar();

       final ArrayList<Restaurant> list = new ArrayList<Restaurant>();
        list.add(new Restaurant(getResources().getDrawable(R.drawable.maitea),"Maitea","Carrer de Casanova, 157, 08036 Barcelona",41.39006,2.15307));
        list.add(new Restaurant(getResources().getDrawable(R.drawable.doscielos),"Dos cielos","Hotel Meliá Barcelona Sky, Carrer de Pere IV, 272 - 286, 08005 Barcelona",41.40626,2.20057));
        list.add(new Restaurant(getResources().getDrawable(R.drawable.intertapa),"Inter Tapa","Av. de Gaudí, 11, 08025 Barcelona",41.40528,2.17429));
        list.add(new Restaurant(getResources().getDrawable(R.drawable.tavernanan),"Taverna del Nan","Carrer Oriental, 10, 08906 L'Hospitalet de Llobregat, Barcelona", 41.36722,2.10247));
        list.add(new Restaurant(getResources().getDrawable(R.drawable.vero),"Pizerria Vero","Av. Pablo Picasso, 8, 08940 Cornellà de Llobregat",41.35493,2.09049));

        BaseAdapter adapter = new RestaurantAdapter(this, list);
        ListView listViewItems = (ListView)findViewById(R.id.lv_restaurantes);
        listViewItems.setAdapter(adapter);
        listViewItems.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                // When clicked, show a toast with the TextView text
                Toast.makeText(getApplicationContext(),"Latitud "+list.get(position).getLatitud()+" Longitud"+list.get(position).getLongitud(), Toast.LENGTH_SHORT).show();
            }
        });

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_restaurant, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        Toast toast=Toast.makeText(this,"",Toast.LENGTH_SHORT);

        //noinspection SimplifiableIfStatement

        return super.onOptionsItemSelected(item);
    }
}