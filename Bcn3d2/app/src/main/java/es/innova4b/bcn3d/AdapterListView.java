package es.innova4b.bcn3d;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Manel Monteserín Navarro on 16/03/2015.
 */
public class AdapterListView extends BaseAdapter{

    protected Activity act;
    protected ArrayList<ObjetoListView> items;

    public AdapterListView(Activity act, ArrayList<ObjetoListView> obj) {
        this.act = act;
        this.items = obj;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = convertView;
        ObjetoListView obj = items.get(position);

        if(convertView == null){
            LayoutInflater inf = (LayoutInflater) act.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inf.inflate(R.layout.itemlistview, null);
        }
        TextView texto = (TextView)view.findViewById(R.id.texto);
        texto.setText(obj.getDireccion());

        TextView tfno = (TextView)view.findViewById(R.id.tfno);
        tfno.setText(obj.getTfno());

        return view;
    }
}
