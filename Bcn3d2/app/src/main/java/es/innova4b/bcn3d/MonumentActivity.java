package es.innova4b.bcn3d;


import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.BaseAdapter;
import android.widget.ListView;

import java.lang.Override;import java.util.ArrayList;


public class MonumentActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lv_monumentos);
        ArrayList<Monumento> monumentos =new ArrayList<Monumento>();
        monumentos.add(new Monumento(getResources().getDrawable(R.drawable.artriunf),"arco del Triunfo","bla bla"));
        monumentos.add(new Monumento(getResources().getDrawable(R.drawable.sagfam),"Sagrada Familia","bla bla"));
        monumentos.add(new Monumento(getResources().getDrawable(R.drawable.colon),"Estatua de Colon","bla bla"));
        monumentos.add(new Monumento(getResources().getDrawable(R.drawable.pedrera),"Pedrera","bla bla"));
        monumentos.add(new Monumento(getResources().getDrawable(R.drawable.pene),"Edificio de las aguas","bla bla"));
        BaseAdapter adapter = new MonumentAdapter(this, monumentos);
        ListView listViewItems = (ListView)findViewById(R.id.lv_monumentos);
        listViewItems.setAdapter(adapter);





    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_monumentos, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
