package es.innova4b.bcn3d;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Manel Monteserín Navarro on 11/03/2015.
 */
public class Servicios extends ActionBarActivity implements View.OnClickListener {

    //List <Blanco> blancos = new ArrayList<Blanco>();
    //List <EditText> textos = new ArrayList<EditText>();
    Button serviciosMetro;
    Button oficinaTurismo;
    Button autobus;
    Button emergencias;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_servicios);

        serviciosMetro = (Button) findViewById(R.id.serviciosMetro);
        serviciosMetro.setBackgroundResource(R.drawable.tmb);
        serviciosMetro.setOnClickListener(Servicios.this);

        oficinaTurismo = (Button) findViewById(R.id.oficinaTurismo);
        oficinaTurismo.setBackgroundResource(R.drawable.turismo);
        oficinaTurismo.setOnClickListener(Servicios.this);

        autobus = (Button) findViewById(R.id.autobus);
        autobus.setBackgroundResource(R.drawable.bus);
        autobus.setOnClickListener(Servicios.this);

        emergencias = (Button) findViewById(R.id.emergencias);
        emergencias.setBackgroundResource(R.drawable.emergencias);
        emergencias.setOnClickListener(Servicios.this);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_servicios, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        Intent ir;
        switch(v.getId()){
            case R.id.serviciosMetro:
                ir = new Intent(Servicios.this, TurismoActivity.class);
                //startActivity(ir);
                break;
            case R.id.oficinaTurismo:
                ir = new Intent(Servicios.this, TurismoActivity.class);
                //startActivity(irDos);
                break;
            case R.id.autobus:
                ir = new Intent(Servicios.this, TurismoActivity.class);
                //startActivity(irDos);
                break;
            case R.id.emergencias:
                ir = new Intent(Servicios.this, TurismoActivity.class);
                break;
            default:
                ir = new Intent(Servicios.this, Servicios.class);
                break;
        }
        startActivity(ir);
    }
}
