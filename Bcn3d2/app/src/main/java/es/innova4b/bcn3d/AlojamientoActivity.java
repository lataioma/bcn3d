package es.innova4b.bcn3d;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Objects;


public class AlojamientoActivity extends Activity {

    private ArrayList<OAlojamiento> listadoAlojamiento;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alojamiento);


        initComponents();

        grabarDatos();

        ListView listviewAloja = (ListView) findViewById(R.id.lvAloja);

        AlojaAdapter adapter = new AlojaAdapter(this, listadoAlojamiento);
        listviewAloja.setAdapter(adapter);

        listviewAloja.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(AlojamientoActivity.this, "prueba", Toast.LENGTH_LONG).show();
            }
        });









/*

        listviewAloja.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Toast.makeText(AlojamientoActivity.this,"prueba", Toast.LENGTH_LONG).show();

               // Toast.makeText(AlojamientoActivity.this, listadoAlojamiento.get(position).getNombre(), Toast.LENGTH_SHORT).show();
               /* Intent intent= new Intent(AlojamientoActivity.this,unAlojamiento.class);
                intent.putExtra("alojamiento",listadoAlojamiento.get(position));
                startActivity(intent);*/
        /*
            }
        });
            */
    }

    private void grabarDatos() {

        listadoAlojamiento.add(new OAlojamiento(
                "Hotel Wela",
                "Plaça de la Rosa dels Vents, 1, 08039 Barcelona",
                2.1929999, 41.368300,
                getResources().getDrawable(R.drawable.majestic)
        ));
        listadoAlojamiento.add(new OAlojamiento(
                "Hotel Majestic",
                "Majestic Hotel & Spa, Passeig de Gràcia, 68 Barcelona",
                2.163981, 41.39351,
                getResources().getDrawable(R.drawable.majestic)
        ));
        listadoAlojamiento.add(new OAlojamiento(
                "Hilton Diagonal Mar Barcelona",
                "Passeig del Taulat 262-264, Barcelona, 08019, España",
                2.217748, 41.40845,
                getResources().getDrawable(R.drawable.mar)
        ));
        listadoAlojamiento.add(new OAlojamiento(
                "Hilton Diagonal Mar Barcelona",
                "Passeig del Taulat 262-264, Barcelona, 08019, España",
                2.217748, 41.40845,
                getResources().getDrawable(R.drawable.mar)
        ));
    }

    private void initComponents() {
        listadoAlojamiento = new ArrayList<>();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_alojamiento, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public class AlojaAdapter extends BaseAdapter {
        protected Context ctx;
        protected ArrayList<OAlojamiento> listado_alojamiento;


        public AlojaAdapter(Context activity, ArrayList<OAlojamiento> listado_alojamiento) {
            this.ctx = activity;
            this.listado_alojamiento = listado_alojamiento;

        }


        @Override
        public int getCount() {
            return listado_alojamiento.size();
        }

        @Override
        public Object getItem(int position) {
            return listado_alojamiento.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = convertView;
            if (convertView == null) {
                LayoutInflater inf = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                view = inf.inflate(R.layout.item_lv_alojamiento, null);
            }

            OAlojamiento alojamiento = listado_alojamiento.get(position);
            ImageView imagen = (ImageView) view.findViewById(R.id.imageView_alojamiento);
            imagen.setImageDrawable(alojamiento.getImagen());

            TextView nombre = (TextView) view.findViewById(R.id.textView_nombre_alojamiento);
            nombre.setText(alojamiento.getNombre());

            TextView direccio = (TextView) view.findViewById(R.id.textView_direccio);
            direccio.setText(alojamiento.getDireccion());

            return view;


        }
    }

}
