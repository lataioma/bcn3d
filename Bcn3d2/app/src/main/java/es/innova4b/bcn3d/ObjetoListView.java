package es.innova4b.bcn3d;

/**
 * Created by Manel Montserín Navarro on 16/03/2015.
 */
public class ObjetoListView {

    private String direccion;
    private String tfno;

    public ObjetoListView(){

    }

    public ObjetoListView(String texto, String tfno) {
        this.direccion = texto;
        this.tfno = tfno;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTfno() {
        return tfno;
    }

    public void setTfno(String tfno) {
        this.tfno = tfno;
    }
}
