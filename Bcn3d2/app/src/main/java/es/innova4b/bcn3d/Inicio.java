package es.innova4b.bcn3d;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;


public class Inicio extends ActionBarActivity {
    Button alojamiento;
    Button servicio;
    Button restuarante;
    Button evento;
    Button monumento;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inicio);
        initComponents();
    }

    private void initComponents() {
        alojamiento=(Button) findViewById(R.id.button_alojamientos);
        alojamiento.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                Intent ir = new Intent(Inicio.this,AlojamientoActivity.class);
                startActivity(ir);
            }
        });
        evento=(Button) findViewById(R.id.button_eventos);
        evento.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
             //   Intent ir = new Intent(Inicio.this,EventosActivity.class);
              //  startActivity(ir);
            }
        });
        monumento=(Button) findViewById(R.id.button_monumentos);
        monumento.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
               // Intent ir = new Intent(Inicio.this,MonumentActivity.class);
                //startActivity(ir);
            }
        });
        servicio=(Button) findViewById(R.id.button_servicios);
        servicio.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                Intent ir = new Intent(Inicio.this,Servicios.class);
                startActivity(ir);
            }
        });
        restuarante=(Button) findViewById(R.id.button_restaurantes);
        restuarante.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                //Intent ir = new Intent(Inicio.this,RestuaranteActivity.class);
                //startActivity(ir);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_inicio, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
      switch (item.getItemId()) {
                case R.id.id_email:
                    Toast.makeText(getApplicationContext(), "Has clicado email", Toast.LENGTH_SHORT).show();
                    return true;
                case R.id.id_off:
                    Toast.makeText(getApplicationContext(), "Has clicado off", Toast.LENGTH_SHORT).show();
                    return true;
                default:
                    return super.onOptionsItemSelected(item);
            }
        }
}
