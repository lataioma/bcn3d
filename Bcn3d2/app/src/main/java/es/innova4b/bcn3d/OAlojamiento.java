package es.innova4b.bcn3d;

import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by usuari on 18/03/2015.
 */
public class OAlojamiento implements Parcelable {
    private String nombre;
    private String direccion;
    private double longitud;
    private double latitude;
    private Drawable imagen;

    public OAlojamiento(String nombre, String direccion, double longitud, double latitude, Drawable imagen) {
        this.nombre = nombre;
        this.direccion = direccion;
        this.longitud = longitud;
        this.latitude = latitude;
        this.imagen = imagen;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public double getLongitud() {
        return longitud;
    }

    public void setLongitud(double longitud) {
        this.longitud = longitud;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public Drawable getImagen() {
        return imagen;
    }

    public void setImagen(Drawable imagen) {
        this.imagen = imagen;
    }

    @Override
    public String toString() {
        return "OAlojamiento{" +
                "nombre='" + nombre + '\'' +
                ", direccion='" + direccion + '\'' +
                ", longitud=" + longitud +
                ", latitude=" + latitude +
                ", imagen=" + imagen +
                '}';
    }

    protected OAlojamiento(Parcel in) {
        nombre = in.readString();
        direccion = in.readString();
        longitud = in.readDouble();
        latitude = in.readDouble();
        imagen = (Drawable) in.readValue(Drawable.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }


    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(nombre);
        dest.writeString(direccion);
        dest.writeDouble(longitud);
        dest.writeDouble(latitude);
        dest.writeValue(imagen);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<OAlojamiento> CREATOR = new Parcelable.Creator<OAlojamiento>() {
        @Override
        public OAlojamiento createFromParcel(Parcel in) {
            return new OAlojamiento(in);
        }

        @Override
        public OAlojamiento[] newArray(int size) {
            return new OAlojamiento[size];
        }
    };
}